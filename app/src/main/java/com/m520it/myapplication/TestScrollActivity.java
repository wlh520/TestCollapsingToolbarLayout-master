package com.m520it.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TestScrollActivity extends AppCompatActivity {
    private static final String TAG = "TestScrollActivity";
    @BindView(R.id.iv_search)
    ImageView ivSearch;

    private float totalHeight;      //总高度
    private float toolBarHeight;    //toolBar高度
    private float offSetHeight;     //总高度 -  toolBar高度  布局位移值
    private float rlHeight;         //搜索框高度
    private float rlWidth;
    private float ivSearchWidth;
    private float offsetWidth;

    private float rlHeightOffScale;     //高度差比值
    private float rlOffDistance;        //距离差
    private float rlOffDistanceScale;   //距离差比值
    private FrameLayout.LayoutParams params;

    @BindView(R.id.toolbar_layout)
    CollapsingToolbarLayout toolbarLayout;
    @BindView(R.id.app_bar)
    AppBarLayout appBar;
    @BindView(R.id.fab)
    EditText fab;
    @BindView(R.id.rl)
    RelativeLayout rl;
    @BindView(R.id.bac)
    TextView bac;
    @BindView(R.id.fl)
    FrameLayout fl;
    @BindView(R.id.image)
    ImageView image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scrolling2);
        ButterKnife.bind(this);


        bac.setAlpha(0f);
        totalHeight = getResources().getDimension(R.dimen.app_bar_height);
        toolBarHeight = getResources().getDimension(R.dimen.tool_bar_height);
        offSetHeight = totalHeight - toolBarHeight;

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(TestScrollActivity.this, TestActivity.class));
            }
        });

        /**
         *   移动效果值／最终效果值 =  移动距离／ 能移动总距离（确定）
         */
        appBar.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            // vertical offset 从滑动动画开始到滑动动画结束的距离
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {

                //第一次进入获取高度，以及差值 高度差比值
                if (rlHeight == 0) {
                    params = (FrameLayout.LayoutParams) rl.getLayoutParams();
//                    The raw measured height of this view.
                    // 这个视图的原始高度
                    rlHeight = rl.getMeasuredHeight();
                    rlWidth = rl.getMeasuredWidth();

                    ivSearchWidth = ivSearch.getMeasuredWidth();
                    offsetWidth = ivSearchWidth;
                    // 父视图的FrameLayout


                    Log.i(TAG, "onOffsetChanged: " + rlHeight);

                    //算出高度偏移量比值  相对与llHeight
                    rlHeightOffScale = 1.0f - (toolBarHeight / rlHeight);

                    //得到滑动差值 就是布局marginTop
                    rlOffDistance = params.topMargin;
                    //得到滑动比值
                    rlOffDistanceScale = rlOffDistance / offSetHeight;
                }

                //滑动一次 得到渐变缩放值
                float alphaScale = (-verticalOffset) / offSetHeight;

                //获取高度缩放值
                float llHeightScale = 1.0f - (rlHeightOffScale * ((-verticalOffset) / offSetHeight));
                //计算margin top值
                float distance = rlOffDistance - (-verticalOffset) * rlOffDistanceScale;

                image.setAlpha(1.0f - alphaScale);
                bac.setAlpha(alphaScale);
                params.height = (int) (rlHeight * llHeightScale);
                params.width = (int) (rlWidth - alphaScale * offsetWidth);
                params.setMargins(0, (int) distance, 0, 0);

                // 更新下下试图
                fl.requestLayout();

            }
        });
    }
}
